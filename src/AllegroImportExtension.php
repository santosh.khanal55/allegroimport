<?php

namespace Bolt\Extension\ProjektDigital\AllegroImport;

use Silex\Application;
use Twig\Markup;
use Bolt\Menu\MenuEntry;
use Bolt\Controller\Zone;
use Bolt\Asset\File\JavaScript;
use Silex\ControllerCollection;
use Bolt\Extension\SimpleExtension;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

class AllegroImportExtension extends SimpleExtension
{
    protected function registerMenuEntries()
    {
        $adminMenuEntry = (new MenuEntry('allegroimport', 'allegroimport'))
            ->setLabel('Allegro Import')
            ->setIcon('fa:university')
            ->setPermission('allegroimport');

        return [$adminMenuEntry];
    }

    protected function registerBackendRoutes(ControllerCollection $collection)
    {
        $collection->get('/extensions/allegroimport', [$this, 'registerBackendTwigMethod']);
        $collection->post('/extensions/allegroimport', [$this, 'postBackendFormSend']);
    }

    public function postBackendFormSend(Application $app, Request $request)
    {
        $il = new ImportLibrary($request->files->get('allegro'));

		$message_category = $il->response == Response::HTTP_BAD_REQUEST ? 'error' : 'info';
        $app['session']->getFlashBag()->set($message_category, $il->message);
        return new RedirectResponse('/bolt/extensions/allegroimport');
    }

    public function registerBackendTwigMethod(Request $request)
    {
        $html = $this->renderTemplate('extension.twig', ['title' => 'Allegro Import']);

        return new Markup($html, 'UTF-8');
    }

    // protected function registerAssets()
    // {
    //     $asset = JavaScript::create('extension.js')->setZone(Zone::BACKEND);
        
    //     return [
    //         $asset
    //     ];
    // }
}
